class DomainConstraint
  def initialize(domain)
    @domains = [domain].flatten
  end
  
  def matches?(request)
    @domains.include? request.host
  end
end

Rails.application.routes.draw do
  root "application#index"

  constraints DomainConstraint.new('perf.vnetwork.vn') do
    get "/dashboard", to: "agent#index"
    resources :campaigns
    resources :sources
  end

  get '/uid/:id', to: "application#export"
  get '/uid/:id/callback', to: "application#callback"

  # Handling Err
  match '*path' => 'application#errors', via: :all
end
