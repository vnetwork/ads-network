class CampaignsController < AgentController
  def index
    @campaigns = JSON.parse Campaign.all.to_json
  end

  def new
    @campaign = Campaign.new
  end

  def edit
    if Campaign.find(id: params[:id]).nil?
      return redirect_to campaigns_path
    end
    
    @campaign = Campaign.find(id: params[:id])
    @sources = JSON.parse Source.all.to_json
  end

  def create
    if params[:campaign][:name].nil?
      return render json: { "message" => "Source name can't be empty." }
    end

    if Campaign.create!(
      name: params[:campaign][:name],
      model: params[:campaign][:model],
      size: params[:campaign][:size],
      zone: params[:campaign][:size],
      target: params[:campaign][:target],
      ads: params[:campaign][:ads]
    )
      redirect_to campaigns_path
    end
  end

  def update
    if Campaign.find(id: params[:id]).nil?
      return redirect_to campaigns_path
    end
    
    @campaign = Campaign.find(id: params[:id])

    if @campaign.update!(
      name: params[:campaign][:name],
      model: params[:campaign][:model],
      size: params[:campaign][:size],
      zone: params[:campaign][:size],
      target: params[:campaign][:target],
      ads: params[:campaign][:ads]
    )
      redirect_to campaigns_path
    end
  end
end
