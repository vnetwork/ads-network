class ApplicationController < ActionController::API
  class Randomize
    def initialize(array)
      @array = array

      @summary = []
      array.each_with_index { |x, i| @summary << (@summary.last || 0) + array[i]["percent"] }
    end

    def draw
      rand = Random.rand()
  
      for i in 0..(@summary.length-1)
        return @array[i]["id"] if rand <= @summary[i]
      end
    end
  end
  
  def index
    render json: { "message" => "Hello" }
  end

  def export
    campaign = fetch_campaigns.find { |i| i["_id"]["$oid"] == params[:id] }

    if campaign.nil?
      return @script = nil
    else
      array = fetch(params[:id])

      if array == []
        return @script = nil
      end

      id = Randomize.new(array).draw
      
      @script = fetch_sources.find { |i| i["_id"]["$oid"] == id }["body"]
      @ad = fetch_sources.find { |i| i["_id"]["$oid"] == id }["_id"]["$oid"]

      # results = []
      # ss = Randomize.new(array)
      # 100.times { results << ss.draw }
      # array.map { |x| { x["id"] => results.count { |y| y == x["id"] }.to_f / results.length } }.reduce(:merge)
      # puts results.group_by{|e| e}.map{|k, v| [k, v.length]}.to_h

      expires_in 1.minutes, public: true, must_revalidate: true
      fresh_when(etag: @script)
    end
  end

  def callback
    if Source.find(id: params[:id]).inc(count: 1)
      render js: "console.log('callback success')"
    end
  end

  def errors
    return render json: { "code" => 404, "message" => "Error. You're looking for nothing." }, status: 404
  end

  private
    def fetch(id)
      array, total = [], 0
      sources = fetch_sources.select { |i| fetch_campaigns.find { |i| i["_id"]["$oid"] == id }["ads"].split(',').include? i["_id"]["$oid"] }
      sources = sources.select { |i| i["schedule_end"] != "" ? i["schedule_end"].to_date == Date.today : i["schedule_end"] == "" }
      sources = sources.select { |i| i["target"] != "" ? i["target"] == request.host : i["target"] == "" }

      if sources.present?
        sources.select { |i| total += i["cost"] }
        
        # sum = total/sources.length
        # max = sources.max {|a,b| a["cost"] <=> b["cost"] }["cost"]

        sources.each do |i|
          array << {
            "id" => i["_id"]["$oid"],
            "percent" => i["cost"]/total
          }
        end
      end

      return array
    end

    def fetch_campaigns
      begin
        campaigns = $redis.get "campaigns"
        if campaigns.nil?
          campaigns = Campaign.all.to_json
          $redis.set "campaigns", campaigns
        end
        campaigns = JSON.load campaigns
      rescue => error
        puts error.inspect
        campaigns = Campaign.all
      end
      campaigns
    end

    def fetch_sources
      begin
        sources = $redis.get "sources"
        if sources.nil?
          sources = Source.all.to_json
          $redis.set "sources", sources
        end
        sources = JSON.load sources
      rescue => error
        puts error.inspect
        sources = Source.all
      end
      sources
    end
end
