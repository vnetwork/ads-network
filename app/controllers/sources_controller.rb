class SourcesController < AgentController
  def index
    @sources = JSON.parse Source.all.to_json

    Source.all.each do |source|
      source.update!(count: 0, updated_at: Time.now) if (Time.now.utc.to_datetime - source.updated_at).to_f > 1
    end
  end

  def new
    @source = Source.new
  end
  
  def edit
    if Source.find(id: params[:id]).nil?
      return redirect_to sources_path
    end

    @source = Source.find(id: params[:id])
  end

  def create
    if params[:source][:name].nil? || params[:source][:body].nil?
      return render json: { "message" => "Required can't be empty" }
    end

    if Source.create!(
      name: params[:source][:name],
      body: params[:source][:body].gsub(/[\r\n]+/,'').gsub('"',"'"),
      size: params[:source][:size],
      zone: params[:source][:size],
      cost: params[:source][:cost],
      target: params[:source][:target],
      schedule_start: (params[:source][:schedule].split(" - ").first.to_date) == (params[:source][:schedule].split(" - ").last.to_date) ? "" : params[:source][:schedule].split(" - ").first, 
      schedule_end: (params[:source][:schedule].split(" - ").first.to_date) == (params[:source][:schedule].split(" - ").last.to_date) ? "" : params[:source][:schedule].split(" - ").last,
      updated_at: Time.now
    )
      redirect_to sources_path
    end
  end

  def update
    if Source.find(id: params[:id]).nil?
      return redirect_to sources_path
    end
    
    @source = Source.find(id: params[:id])

    if @source.update!(
      name: params[:source][:name],
      body: params[:source][:body].gsub(/[\r\n]+/,'').gsub('"',"'"),
      size: params[:source][:size],
      zone: params[:source][:size],
      cost: params[:source][:cost],
      target: params[:source][:target],
      schedule_start: (params[:source][:schedule].split(" - ").first.to_date) == (params[:source][:schedule].split(" - ").last.to_date) ? "" : params[:source][:schedule].split(" - ").first, 
      schedule_end: (params[:source][:schedule].split(" - ").first.to_date) == (params[:source][:schedule].split(" - ").last.to_date) ? "" : params[:source][:schedule].split(" - ").last,
      updated_at: Time.now
    )
      redirect_to sources_path
    end
  end

  def destroy
    if Source.find(id: params[:id]).destroy
      redirect_to sources_path
    end
  end
end
