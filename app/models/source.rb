class Source
  include Mongoid::Document
  field :name, type: String
  field :body, type: String
  field :size, type: Integer
  field :zone, type: Integer
  field :cost, type: Float
  field :target, type: String
  field :schedule_start, type: String
  field :schedule_end, type: String
  field :count, type: Integer
  field :updated_at, type: DateTime

  after_save :clear_cache
  after_destroy :clear_cache
  
  private
	  def clear_cache
	    $redis.del "sources"
	  end
end