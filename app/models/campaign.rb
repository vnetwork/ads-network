class Campaign
  include Mongoid::Document
  field :name, type: String
  field :model, type: Integer
  field :size, type: Integer
  field :zone, type: Integer
  field :target, type: String
  field :ads, type: String

  after_save :clear_cache
  after_destroy :clear_cache
  
  private
	  def clear_cache
	    $redis.del "campaigns"
	  end
end