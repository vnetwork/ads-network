// jQuery 3.x
//= require jquery3

// Basic Rails
//= require rails-ujs
//= require turbolinks

// Bootstrap 4.x
//= require popper
//= require bootstrap

//= require select2.min
//= require axios.min