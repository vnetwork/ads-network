module ApplicationHelper
  def campaignModel(id)
    case id
      when 1
        'CPM'
      when 2
        'CPC'
    end
  end

  def campaignZone(id)
    case id
      when 1
        'Top'
      when 2
        'Bottom'
      when 3
        'Left'
      when 4
        'Right'
      when 5
        'Center'
    end
  end

  def campaignSize(id)
    case id
      when 1
        '300x250'
      when 2
        '728x90'
      when 3
        '160x600'
      when 4
        '300x600'
      when 5
        '320x50'
      when 6
        '970x90'
      when 7
        '120x600'
    end
  end
end
